// Transaction.java
// Abstract superclass Transaction represents an ATM transaction

public abstract class Transaction
{
   private int accountNumber; // indicates account involved
   private Screen screen; // ATM's screen
   private BankDatabase bankDatabase; // account info database
   private GUI gui;

   // Transaction constructor invoked by subclasses using super()
   public Transaction( int userAccountNumber, Screen atmScreen,
      BankDatabase atmBankDatabase, GUI gui )
   {
      accountNumber = userAccountNumber;
      screen = atmScreen;
      bankDatabase = atmBankDatabase;
      this.gui = gui;
   } // end Transaction constructor

   // return account numberxw
   public int getAccountNumber()
   {
      return accountNumber;
   } // end method getAccountNumber

   // return reference to screen
   public Screen getScreen()
   {
      return screen;
   } // end method getScreen

   // return reference to bank database
   public BankDatabase getBankDatabase()
   {
      return bankDatabase;
   } // end method getBankDatabase

   public GUI getGUI()
   {
       return gui;
   }
   // perform the transaction (overridden by each subclass)
   abstract public void execute();
} // end class Transaction

