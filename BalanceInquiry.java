// BalanceInquiry.java
// Represents a balance inquiry ATM transaction

public class BalanceInquiry extends Transaction
{
	private Screen screen;
	private GUI gui;
   // BalanceInquiry constructor
   public BalanceInquiry( int userAccountNumber, Screen atmScreen,
      BankDatabase atmBankDatabase, GUI gui )
   {
      super( userAccountNumber, atmScreen, atmBankDatabase, gui );
   } // end BalanceInquiry constructor

   // performs the transaction
   public void execute()
   {
      // get references to bank database and screen
      BankDatabase bankDatabase = getBankDatabase();

      // get the available balance for the account involved
      double availableBalance =
         bankDatabase.getAvailableBalance( getAccountNumber() );

      // get the total balance for the account involved
      double totalBalance =
         bankDatabase.getTotalBalance( getAccountNumber() );

      gui = getGUI();
      screen = getScreen();

      //Button choices
      gui.mainMenuButtonAction("_BLANK");

      // display the balance information on the screen
      screen.displayMessage(gui, "Loading...");

      screen.mergeMessage( gui, "\nPress EXIT to leave\n" );
      screen.mergeMessage( gui, "\nBalance Information:\n" );
      screen.mergeMessage( gui, " - Available balance: \n$" );
      screen.mergeMessage( gui, Double.toString(availableBalance));
      screen.mergeMessage( gui, "\n - Total balance:     \n" );
      screen.displayDollarAmount( gui, totalBalance );
      int returnChoice = gui.waitChoice();
			if (returnChoice == 0) {
				return;
			}
   } // end method execute

} // end class BalanceInquiry

