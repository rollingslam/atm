// CashDispenser.java
// Represents the cash dispenser of the ATM

public class CashDispenser
{
   // the default initial number of HKD in the cash dispenser
   private final static int INITIAL_COUNT = 100;
   private int count; // number of $100 HKD remaining

   // no-argument CashDispenser constructor initializes count to default
   public CashDispenser()
   {
      count = INITIAL_COUNT; // set count attribute to default
   } // end CashDispenser constructor

   // simulates dispensing of specified amount of cash
   public void dispenseCash( int amount )
   {
      int billsRequired = amount / 100; // number of $100 HKD required
      count -= billsRequired; // update the count of HKD
   } // end method dispenseCash

   // indicates whether cash dispenser can dispense desired amount
   public boolean isSufficientCashAvailable( int amount )
   {
      int billsRequired = amount / 100; // number of $100 HKD required

      if ( count >= billsRequired  )
         return true; // enough HKD available
      else
         return false; // not enough HKD available
   } // end method isSufficientCashAvailable
} // end class CashDispenser

