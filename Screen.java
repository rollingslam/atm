// Screen./java
// Represents the screen of the ATM

public class Screen
{
   // displays a message without a carriage return
   public void displayMessage( GUI gui, String message )
   {
      //gui.setMessage("");
      gui.setMessage( message ) ;
      gui.printMessage();


   } // end method displayMessage

   //concate the message to gui.message
   public void mergeMessage( GUI gui, String message ) {
	   gui.mergeMessage( message );
   }

   // display a dollar amount
   public void displayDollarAmount( GUI gui, double amount )
   {
      gui.mergeMessage( String.format( "$%,.2f", amount ));
      gui.printMessage();
   } // end method displayDollarAmount
} // end class Screen

